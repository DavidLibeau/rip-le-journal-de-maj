<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>

        <body>
            <h2>Journal</h2>
            <ul>
                <xsl:for-each select="//entry">
                    <li>
                        <xsl:attribute name="class">
                            <xsl:value-of select="@type" />
                        </xsl:attribute>
                        <div class="date">
                            <xsl:value-of select="date" />
                        </div>
                        <xsl:choose>
                            <xsl:when test="@type = 'maj'">
                                <h3>Mise à jour <xsl:value-of select="version" /></h3>
                                <ul>
                                    <xsl:for-each select="./changelog/li">
                                        <li>
                                            <xsl:value-of select="text()" />
                                        </li>
                                    </xsl:for-each>
                                    <xsl:if test="count(./changelog/li) = 0">
                                        <li>
                                            <em>Pas d'information disponible</em>
                                        </li>
                                    </xsl:if> 
                                </ul>
                            </xsl:when>
                            <xsl:when test="@type = 'info'">
                                <h3>Information</h3>
                                <p><xsl:value-of select="description" /></p>
                            </xsl:when>
                            <xsl:when test="@type = 'incident'">
                                <h3>Incident</h3>
                                <p><xsl:value-of select="description" /></p>
                            </xsl:when>
                            <xsl:otherwise>
                                <h3>Erreur</h3>
                            </xsl:otherwise>
                        </xsl:choose>
                    </li>
                </xsl:for-each>
            </ul>
        </body>

        </html>
    </xsl:template>

</xsl:stylesheet>